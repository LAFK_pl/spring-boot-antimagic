package pl.lafk.starszybrat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * User REST controller
 *
 * @author Tomasz @LAFK_pl Borek
 */
@RestController
class Controller {

    @Autowired
    private UserRepo repo;

    @GetMapping("/perName/{name}")
    public User perName(@PathVariable String name) {
        return repo.findByName(name);
    }

    @PostMapping("/add/")
    public boolean insert(@RequestBody User user) {
        final User saved = repo.save(user);
        return saved == user;
    }
}
