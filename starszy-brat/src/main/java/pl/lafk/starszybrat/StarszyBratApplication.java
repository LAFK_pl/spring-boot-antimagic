package pl.lafk.starszybrat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StarszyBratApplication implements CommandLineRunner {

    @Autowired
    private UserRepo repo;

	public static void main(String[] args) {
		SpringApplication.run(StarszyBratApplication.class, args);
	}

    @Override
    public void run(String... args) throws Exception {
        repo.save(new User(null, 25, "TJB"));
        repo.save(new User(null, 21, "Zo"));
    }
}

