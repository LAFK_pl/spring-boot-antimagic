package pl.lafk.starszybrat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * User entity
 *
 * @author Tomasz @LAFK_pl Borek
 */
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;
    int age;
    String name;
}
