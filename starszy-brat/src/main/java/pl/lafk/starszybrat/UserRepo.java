package pl.lafk.starszybrat;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Repository;

import java.util.List;

@EnableJpaRepositories
@Repository
public interface UserRepo extends JpaRepository<User, Long> {
    User findByName(String name);
    List<User> findTopByAge(int limit);
    List<User> findUsersByAgeGreaterThanEqual(int ceiling);

}
