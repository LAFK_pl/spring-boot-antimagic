package pl.lafk.zabawa;

import java.util.Arrays;
import java.util.Base64;

/**
 * Rozpoznanie API JDK do tegoż kodowania
 *
 * @author Tomasz @LAFK_pl Borek
 */
class KodowanieNaBazie64 {

    public static void main(String[] args) {
        zabawmySię("admin");
        zabawmySię("admi");
        zabawmySię("adm");
        zabawmySię("ad");
        zabawmySię("a");
        zabawmySię("aa");
        zabawmySię("aaa");
        zabawmySię("aaaa");
        zabawmySię("aaaaa");
    }

    private static void zabawmySię(String napis) {
        final byte[] bajty = napis.getBytes();
        System.out.println(napis);
        System.out.println(Arrays.toString(bajty));
        System.out.println(Arrays.toString(Base64.getEncoder().encode(bajty)));
        System.out.println(Base64.getEncoder().encodeToString(bajty));
    }
}
