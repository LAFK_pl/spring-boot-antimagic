package pl.lafk;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;

/**
 * Default configuration for launching Tomcat
 *
 * @author Tomasz @LAFK_pl Borek
 */
@Configuration
public class DefaultConfig {

    @Bean
    @Conditional(TomcatOnClassPathCondition.class)
    public TomcatLauncher launch() {
        return new TomcatLauncher();
    }
}