package pl.lafk;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;

/**
 * @author Tomasz @LAFK_pl Borek
 */
class HelloServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.getWriter().println("<HTML><TITLE>juppi</TITLE><H1>"+ LocalDateTime.now()+"</H1></HTML>");
    }
}
