package pl.lafk;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * Stawia kontekst Springa. Ten odpala Tomcata.
 *
 * @author Tomasz @LAFK_pl Borek
 */
public class App {

    @Configuration
    @Import(DefaultConfig.class)
    public static class MojaKonfiguracja {

    }

    public static void main(String[] args) {
        new AnnotationConfigApplicationContext(MojaKonfiguracja.class);
    }
}
