package pl.lafk;

import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

/**
 * @author Tomasz @LAFK_pl Borek
 */
class TomcatOnClassPathCondition implements Condition {

    @Override
    public boolean matches(ConditionContext conditionContext, AnnotatedTypeMetadata annotatedTypeMetadata) {
        try {
            Class.forName("org.apache.catalina.startup.Tomcat");
        } catch (ClassNotFoundException e) {
            System.err.println("nie załadowałem klasy " + e.getMessage());
            return false;
        }
        return true;
    }
}
