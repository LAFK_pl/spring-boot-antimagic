package pl.lafk;

import org.apache.catalina.Context;
import org.apache.catalina.LifecycleException;
import org.apache.catalina.startup.Tomcat;

import javax.annotation.PostConstruct;

/**
 * Odpala Tomkata. Jest czymś co można wstrzyknąć.
 *
 * @author Tomasz @LAFK_pl Borek
 */
class TomcatLauncher {

    @PostConstruct
    void start() throws LifecycleException {
        System.out.println("Jak odpalić Tomkata?");
        Tomcat tomcat = new Tomcat();
        tomcat.setPort(8080);
        final Context ctx = tomcat.addContext("", null);
        Tomcat.addServlet(ctx, "siemano", new HelloServlet());
        ctx.addServletMappingDecoded("/", "siemano");
        tomcat.start();

        new Thread( () -> tomcat.getServer().await(), "tomkacie-żyj!").start();

    }
}
